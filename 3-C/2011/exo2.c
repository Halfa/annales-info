#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#define TAILMAX 6

int* f2(void) {
    int table[] = {5,4,3,2,1,0};
    int *ptrcour, *ptri;
    int i, nbOctets;

    ptrcour = table + TAILMAX;

    printf("%d %d %d\n", (int) (ptrcour-table), (int) *table, *(ptrcour-1)); // 6 5 0

    ptrcour = ptri = (int *) calloc(TAILMAX, sizeof(int));

    for(i = 0; i < TAILMAX; i++) {
        *(ptrcour++) = TAILMAX - i - 1;
        printf("%d ", ptri[i]);
    }

    nbOctets = sizeof(int) * (TAILMAX+5);
    ptri = (int *) realloc(ptri, nbOctets);

    printf("\n%d\n", (int) (nbOctets / sizeof(table[0])));  // 11
    return ptri;

}

int main(void) {
    int *ptrEntier = (int *) NULL;
    f2();
    return 0;
}