#include <stdio.h>
#include <stdlib.h>
#define R1 ((char)'R')

void f1(void) {
    const char R2 = 'S';
    const char carac[5] = {'A', 'B', 'C', 'D', 'E'};
    const int table[5] = {81,82,3,4,5};
    int a=3,b=2,c=1;

    printf("Q1 : %c %c %d %d\n", R1, R2, table[1], carac[table[4]]);
    printf("Q2 : %c %c %c %c\n", R1, R2, table[1], carac[table[3]]);

    if((a=b) || (a=c++))
        a = a+b++;

    printf("Q3 : %d %d %d\n", a, b, c);

    if((b=c*b)>c)
        c += a + ++b;

    printf("Q4 : %d %d %d\n", a, b, c);

    c = a*5 + ((a << 2) & b);

    printf("Q5 : %d %d %d\n", a, b, c);
}

int main(void) {
    f1();
    return 0;
}