Correction C 2011
=================

Les fichiers de vérification sont fournis, ainsi qu'un makefile.

**Note :** La commande `make` compile tous les exercices sous les noms `exo1`, `exo2`, `exo4`, `various`

Question | Réponse | Commentaire
---------|---------|-----------------------------------
1        | D       | table[4] = 5 - carac[5] = on sort du tableau - Donne R S 82 ?
2        | C       | R S R E car on a mis %c, donc on utilise la table ASCII pour convertir la valeur numérique 82 en R. 
3        | A       | a=b affectation de variable. a == b comparaison. En C si var differente de 0 -> renvoie VRAI. Dans le cas de x || y, si x vrai alors y pas executé. a++ -> on envoie la valeur puis incremente. ++a -> on incremente puis on envoie la valeur
4        | C       | 
5        | A       |
6        | B       |
7        | D       |
8        | A       |
9        | A       |
10       | D       |
11       | B       | Unused Variable
12       | B       | Pas sûr.
13       | B       | Pas sûr.
14       | B       | Pas sûr.
15       | A       | 
16       | C       |
17       | A       |
18       | B       |
19       | B       |
20       | C       |
21       | C       |
22       | B       |
23       | B       |
24       | D       |
25       | D       |
26       | B       |
27       | B       |
28       | C       |
29       | D       |
30       | A       |
